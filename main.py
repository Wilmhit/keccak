import random
from scipy.special import comb
import numpy as np
from collections import Counter
import math
import os
import scipy.stats as st
import statistics
from bitarray import bitarray
from bitarray.util import int2ba

word_size = 512
input = ''
#function = os.system("main")
function = ''

for _ in range(word_size):
    input += str(random.randint(0, 1))
    function += str(random.randint(0, 1))

# zapis do input.bin

def binary_save(s):
    return int(s, 2).to_bytes((len(s) + 7) // 8, byteorder='big')

with open("input.bin", "wb") as file:
    file.write(binary_save(input))

# odczyt z pliku jako parametr funkcji

def binary_read(bit_file):
    inputBitarray = bitarray()
    with open(bit_file, 'rb') as readFile:
        inputBitarray.fromfile(readFile)
    return inputBitarray

readF = binary_read("input.bin").to01()

# prezentacja udanego kodowania i odkodowania

print("-----Coding-----")
print(input)
print(binary_save(input))
print(readF)


# print(input, function)

print("-----SAC-----")

# wektory przesunięcia

lista = []
vectors = []

for i in range(word_size):
    lista.append('0' * word_size)

for i in range(word_size):
    string = lista[i]
    position = i
    new_character = '1'
    string = string[:position] + new_character + string[position + 1:]
    vectors.append(string)

vectors = ['0' * word_size] + vectors

# xor każdego wektora z inputem
vectors2 = []
for i in vectors:
    x = int(i, 2) ^ int(input, 2)
    i = '{0:b}'.format(x)
    vectors2.append(i)
# print(vectors2)

# uruchomienie programu main z każdym przesuniętym inputem, aby móc porównać outputy

inputs = vectors2
outputs = []
for i in range(word_size):
    # output = os.system("main")


    output = ''
    for _ in range(word_size):
        output += str(random.randint(0, 1))
    outputs.append(str(output))


# sprawdzanie z poprzednim różnicy bitów

def bit_diff(a, b):
    diff = 0
    length = min(len(a), len(b))
    for k in range(length):
        if a[k] != b[k]:
            diff += 1
    return diff

diff_list = []
for i in range(word_size):
    bits = bit_diff(outputs[i], outputs[i-1])
    diff_list.append(bits)

# print(diff_list)
x = sum(diff_list)
sac = (x/(512*512)) * 100
print('{:0.3f}'.format(sac), '%')
print("min =", '{:0.3f}'.format(min(diff_list) / 512 * 100), '%, max =', '{:0.3f}'.format(max(diff_list) / 512 * 100), '%')

# poker test

def PokerTest(s, m):
    X2theoretical = [3.84, 5.99, 7.81, 9.48, 11.07, 12.59, 14.06]
    k = len(s) // m
    l = list(np.arange(0, k))
    s1 = s
    for i in range(0, k):
        while len(s1) > 0:
            l[i] = s1[:m]
            s1 = s1[m:]
            break
    n = l
    for j, i in enumerate(l):
        try:
            n[j] = Counter(i)['1']
        except:
            n[j] = Counter(i)['0']

    n.sort()
    niDict = dict(Counter(n))
    k = [i for i in range(0, m + 1)]
    dummydict = dict(zip(k, [0] * len(k)))

    def check_existence(i, collection: iter):
        return i in collection

    if dummydict.keys() == niDict.keys():
        dummy = 0
        # print('ok')
    else:
        for i in dummydict.keys():
            if check_existence(i, niDict.keys()) == False:
                niDict[i] = 0
    b = []

    for i in niDict.keys():
        numerator = math.pow(niDict[i] - comb(m, i) * len(s) / ((2 ** m) * m), 2)
        denominator = comb(m, i) * len(s) / ((2 ** m) * m)
        S = numerator / denominator
        b.append(S)

    X2 = sum(b)
    if X2 < X2theoretical[m - 1]:
        return 1
    else:
        return 0

print("-----Poker test-----")

outputs = []
success = 0
for i in range(word_size):
    # output = os.system("main")


    output = ''
    for _ in range(word_size):
        output += str(random.randint(0, 1))
    outputs.append(str(output))

for i in range(10):
        success += PokerTest(outputs[i], 4)

print("Succeeded outputs:", success/10 * 100, '%')


# test serii

for i in range(word_size):
    # output = os.system("main")


    output = ''
    for _ in range(word_size):
        output += str(random.randint(0, 1))
    outputs.append(str(output))


def getRuns(l):
    runsList = []
    tmpList = []
    for i in l:
        if len(tmpList) == 0:
            tmpList.append(i)
        elif i == tmpList[len(tmpList) - 1]:
            tmpList.append(i)
        elif i != tmpList[len(tmpList) - 1]:
            runsList.append(tmpList)
            tmpList = [i]
    runsList.append(tmpList)

    return len(runsList), runsList

def WW_runs_test(R, n1, n2, n):
    seR = math.sqrt(((2*n1*n2) * (2*n1*n2 - n)) / ((n**2)*(n-1)))
    muR = ((2*n1*n2)/n) + 1
    z = (R - muR) / seR

    return z

z_values = []
p_oneside = []
p_twoside = []

for i in outputs:
    L1 = list(i)
    L = []
    for i in L1:
        l = int(i)
        L.append(l)
    numRuns, listOfRuns = getRuns(L)
    R = numRuns
    n1 = sum(L)
    n2 = len(L) - n1
    n = n1 + n2

    ww_z = WW_runs_test(R, n1, n2, n)

    p_values_one = st.norm.sf(abs(ww_z))  # one-sided
    p_values_two = st.norm.sf(abs(ww_z)) * 2  # twosided

    z_values.append(ww_z)
    p_oneside.append(p_values_one)
    p_twoside.append(p_values_two)

print("-----Series test-----")
print("z value:", statistics.mean(z_values))

# balance

outputs = []

for i in range(word_size):
    # output = os.system("main")


    output = ''
    for _ in range(word_size):
        output += str(random.randint(0, 1))
    outputs.append(str(output))

zero = 0
ones = 0
for i in outputs:
    zero += i.count('0')
    ones += i.count('1')
print("-----Balance-----")
print('0:', round((zero/word_size)/word_size * 100, 3), '%', ',1:', round((ones/word_size)/word_size * 100, 3), '%')

# kolizje
# take your sample input values, hash each one and put the results into a set. Count the size of the resulting set and compare it to the size of the input set

distinct_outputs = []

for i in range(10000):
    # output = os.system("main")


    output = ''
    for _ in range(word_size):
        output += str(random.randint(0, 1))
    outputs.append(str(output))


    if output not in distinct_outputs:
        distinct_outputs.append(output)
collisions = abs(len(distinct_outputs) - 10000)
print("-----Collisions-----")
print('{:0.3f}'.format(collisions/100), '%')

