/**
  This file is part of keccak implementation at gitlab.com/wilmhit/keccak
  It is licensed under GNU GPLv3
*/
#include "libstate.c"
#include <criterion/hooks.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>
#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

// https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
#define PRINT_BIN "%c%c%c%c%c%c%c%c"
#define BYTE_BIN(byte)                                                         \
  (byte & 0x80 ? '1' : '0'), (byte & 0x40 ? '1' : '0'),                        \
      (byte & 0x20 ? '1' : '0'), (byte & 0x10 ? '1' : '0'),                    \
      (byte & 0x08 ? '1' : '0'), (byte & 0x04 ? '1' : '0'),                    \
      (byte & 0x02 ? '1' : '0'), (byte & 0x01 ? '1' : '0')

u_char swapped[] = {
    0x80, 0x80, 0x80, 0x80, 0x80, //
    0x80, 0x80, 0x80, 0x80, 0x80, //
    0x80, 0x80, 0x80, 0x80, 0x80, //
    0x80, 0x80, 0x80, 0x80, 0x80, //
    0x80, 0x80, 0x80, 0x80, 0x80, //
};

u_char unswapped[] = {
    0xff, 0xff, 0xff, 0x80, 0x00, //
    0x00, 0x00, 0x00, 0x00, 0x00, //
    0x00, 0x00, 0x00, 0x00, 0x00, //
    0x00, 0x00, 0x00, 0x00, 0x00, //
    0x00, 0x00, 0x00, 0x00, 0x00, //
};

// void print_state(state *state);

// void print_state(state *state) {
//   FOR_EVERY_SLICE {
//     printf("==============;\n");
//     for (int y = 0; y < STATE_Y_LEN; y++) {
//       for (int x = 0; x < STATE_X_LEN; x++) {
//         size_t index = slice_num * STATE_SLICE_LEN + y * STATE_X_LEN + x;
//         printf("0x%08x,", (u_int)state->array[index]);
//       }
//       printf("\n");
//     }
//   }
// }

Test(state, init) { struct State s = get_state(10); }

Test(padding, simple) {
  u_char *buf = malloc(200);
  strcpy((char *)buf, "aaad");
  // buf[4] = 0; // so that string is null-terminated after 4 u_chars

  padd_input(buf, 200);
  cr_assert(buf[4] == 0x80);
  cr_assert(buf[199] == 0x01);
  free(buf);
}

Test(theta, simple) {
  state s = get_state(512);
  s.array[0] = (u_char)255; // setting first 8 bits of first word to "1"

  theta(&s);
  // now first 8 bits of every word in second column as well as last column
  // should be "1"

  for (int i = 0; i < STATE_SLICE_LEN; i += STATE_X_LEN) {
    cr_assert(s.array[i + 1] == (u_char)255);       // first column
    cr_assert(s.array[200 - i - 1] == (u_char)255); // last column
  }
}

Test(rho, circular_shift) {
  u_char to_shift = (u_char)0xfc;
  u_char reminder = circular_shift(&to_shift, 0xa0, 4);
  cr_assert(reminder == (u_char)0xf0);
  cr_assert(to_shift == (u_char)0xca);
}

Test(rho, simple) {
  state s = get_state(512);
  // First word should not move
  s.array[0] = 0x20; // this should not move
  rho(&s);
  cr_assert(s.array[0] == (u_char)0x20);
}

Test(rho, second_word) {
  state s = get_state(512);

  // Second word should move by one bit
  s.array[1] = 0x80;   // first byte 1000 000 => 0000 0001
  s.array[26] = 0x80;  // second byte 1000 0000 => 0000 0000
  s.array[176] = 0x00; // last byte 0000 0000 => 0000 0001

  rho(&s);

  cr_assert(s.array[1] == (u_char)0x01);
  cr_assert(s.array[26] == (u_char)0x00);
  cr_assert(s.array[176] == (u_char)0x01);
}

Test(pi, index_3d) {
  cr_assert(get_3d_index(0, 1, 3) == 8);
  cr_assert(get_3d_index(7, 6, 3) == 8);
}

Test(pi, simple) {
  state s = get_state(512);
  for (int i = 0; i < STATE_SIZE_BYTES; i += STATE_SLICE_LEN) {
    s.array[i] = 0x11;     // First word == [0][0]
    s.array[i + 7] = 0x33; // Third word in second row  == [1][2]
  }
  s = pi(&s);
  for (int i = 0; i < STATE_SIZE_BYTES; i += STATE_SLICE_LEN) {
    cr_assert(s.array[i] == 0x11);
    /*
      [1][2] => [3*1 + 2*2][1] = [7][1] = [2][1]
    */
    cr_assert(s.array[(2 * STATE_X_LEN) + 1] == 0x33);
  }
}

Test(chi, simple) {
  state s = get_state(512);
  // s[i] ^= ~s[i_below] & s[i_below_below]
  // 1 ^ (~0 & 1) = 1 ^ (1 & 1) = 1 ^ 1 = 0
  s.array[0] = 0x01;
  s.array[5] = 0x00;
  s.array[10] = 0x01;
  chi(&s);
  cr_assert(s.array[0] == 0x00);
}

Test(misc, unswapp_bits) {
  u_char *result = unswapp_bits(swapped, 25);
  for (int i = 0; i < 25; i++) {
    cr_assert(unswapped[i] == result[i]);
  }
}

Test(misc, set_bit) {
  u_char b2 = 0x80;
  u_char b1 = 0xa0;
  set_bit(&b1, &b2, 6, 0);
  cr_assert(b1 == 0xa2);
}

Test(misc, swapp_bits) {
  state s = get_state(512);
  swapp_bits(&s, unswapped, 25);
  for (int i = 0; i < 25; i++) {
    cr_assert(swapped[i] == s.array[i]);
  }
}
