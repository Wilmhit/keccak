/**
  This file is part of keccak implementation at gitlab.com/wilmhit/keccak
  It is licensed under GNU GPLv3
*/
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#define R_BITS 576
#define R_BYTES (R_BITS / 8)
#define STATE_SIZE_BITS 1600
#define STATE_SIZE_BYTES (STATE_SIZE_BITS / 8)
#define STATE_X_LEN 5
#define STATE_Y_LEN 5
#define STATE_SLICE_LEN (STATE_Y_LEN * STATE_X_LEN)
#define TOTAL_SLICES (STATE_SIZE_BITS / (STATE_X_LEN * STATE_Y_LEN))
#define L 6
#define WORD 64 // 2.power(L)
#define OUTPUT_SIZE_BITS 512
#define FOR_EVERY_SLICE                                                        \
  for (int slice_num = 0; slice_num < TOTAL_SLICES / 8; slice_num++)

typedef struct State {
  u_char array[STATE_SIZE_BYTES];
  size_t r_bits;
} state;

inline const u_int get_next_triangular(size_t *index);
void absorbing_iteration(state *state, u_char *plaintext_part);
void permutate(state *state);
void xor_r(state *state, u_char *plaintext_part);
u_char *get_r_cpy(state state);
u_char get_cell(state s, int x, int y, int z);
state get_state(size_t r_size);
void theta(state *state);
void rho(state *state);
state pi(state *s);
void chi(state *state);
void iota(state *state, int round);
void padd_input(u_char *buffer, size_t buff_len);
u_char circular_shift(u_char *b, u_char front_fill, u_int shift);
size_t get_3d_index(size_t slice, size_t y, size_t x);
u_int64_t xorshift(u_int64_t xs);
u_char *unswapp_bits(u_char *state_part, size_t len);
void swapp_bits(state *s, u_char *unswapped, size_t len);
