/**
  This file is part of keccak implementation at gitlab.com/wilmhit/keccak
  It is licensed under GNU GPLv3
*/
#include "libstate.h"
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>

const u_int triangular_numbers[] = {
    0,   1,   3,   6,   10,  15,  21,  28,  36,  45,  55,  66,  78,
    91,  105, 120, 136, 153, 171, 190, 210, 231, 253, 276, 300, 325,
    351, 378, 406, 435, 465, 496, 528, 561, 595, 630, 666};

inline const u_int get_next_triangular(size_t *index) {
  u_int number = triangular_numbers[*index];
  index++;
  return number;
}

/// First XOR then permutate
void absorbing_iteration(state *state, u_char *plaintext_part) {
  swapp_bits(state, plaintext_part, R_BYTES);
  permutate(state);
}

/// This is permutation block
void permutate(state *state) {
  // https://en.wikipedia.org/wiki/SHA-3#The_block_permutation

  int permutations_total = 12 + (2 * L);
  for (int i = 0; i < permutations_total; i++) {
    theta(state);
    rho(state);
    *state = pi(state);
    chi(state);
    iota(state, i);
  }
}

/// XOR the R part of `state` with `plaintext_part` inplace
void xor_r(state *state, u_char *plaintext_part) {
  // TODO this needs bit swapping
  for (int i = 0; i < R_BYTES; i++) {
    state->array[i] ^= plaintext_part[i];
  }
}

/// This is a helper function used in tests
u_char *get_r_cpy(state state) {
  u_char *new_buff = malloc(ceil(state.r_bits / 8.0));
  return memcpy(new_buff, state.array, state.r_bits);
}

/// Create new empty state
state get_state(size_t r_size) {
  state s = {0}; // equivalent to memset(s, 0, sizeof(s));
  s.r_bits = r_size;
  return s;
}

// Calculates parity bits for 8 columns (u_char length)
u_char get_parity_for_columns(state *state, size_t start_byte_index) {
  u_char xored = 0;
  int end_index = start_byte_index + STATE_X_LEN * STATE_Y_LEN;
  for (int i = start_byte_index; i < end_index; i += STATE_X_LEN) {
    xored ^= state->array[i];
  }
  return xored;
}

void theta(state *state) {
  // Generating parities
  size_t parity_arr_len = (TOTAL_SLICES * STATE_X_LEN) / 8;
  u_char *parity = malloc(parity_arr_len);
  FOR_EVERY_SLICE {
    for (int x = 0; x < STATE_X_LEN; x++) {
      // x is the index of a byte belonging to first word in a column
      parity[(slice_num * STATE_X_LEN) + x] =
          get_parity_for_columns(state, (slice_num * STATE_SLICE_LEN) + x);
    }
  }

  // XORing parities
  FOR_EVERY_SLICE {
    for (int x = 0; x < STATE_X_LEN; x++) {
      size_t next_column_index =
          ((slice_num * STATE_X_LEN) + x + 1) % parity_arr_len;
      size_t prev_column_index =
          (((slice_num * STATE_X_LEN) + x - 1) + parity_arr_len) %
          parity_arr_len;

      for (int y = 0; y < STATE_Y_LEN; y++) {
        size_t index = (slice_num * STATE_SLICE_LEN) + (y * STATE_X_LEN) + x;
        state->array[index] ^= parity[next_column_index];
        state->array[index] ^= parity[prev_column_index];
      }
    }
  }
  free(parity);
}

/// Returns reminder from the end
u_char circular_shift(u_char *b, u_char front_fill, u_int shift) {
  u_char reminder = *b >> (8 - shift);
  reminder <<= (8 - shift);

  u_char mask = 255 << shift; // mask like in IP addresses
  front_fill >>= (8 - shift);
  front_fill &= ~mask;

  *b <<= shift;
  *b ^= front_fill;
  return reminder;
}

void close_circle(u_char reminder, u_char *last_byte, u_int shift) {
  reminder >>= 8 - shift;
  *last_byte ^= reminder;
}

void rho(state *state) {
  for (int i = 0; i < STATE_SLICE_LEN; i++) {
    u_int shift = triangular_numbers[i];
    u_char reminder = 0;
    FOR_EVERY_SLICE {
      int reverse_iter =
          (TOTAL_SLICES / 8 - slice_num - 1) * STATE_SLICE_LEN + i;
      u_char *byte = &state->array[reverse_iter];
      reminder = circular_shift(byte, reminder, shift);
    }
    u_char *last_byte =
        &state->array[STATE_SIZE_BITS / 8 + i - STATE_SLICE_LEN];
    close_circle(reminder, last_byte, shift);
  }
}

size_t get_3d_index(size_t slice, size_t y, size_t x) {
  size_t slice_offset = slice * STATE_SLICE_LEN;
  size_t y_offset = y * STATE_X_LEN;
  size_t to_ret = slice_offset + y_offset + x;
  return to_ret % STATE_SIZE_BYTES;
}

state pi(state *s) {
  state s_new = get_state(s->r_bits);

  for (int x = 0; x < STATE_X_LEN; x++) {
    for (int y = 0; y < STATE_Y_LEN; y++) {
      FOR_EVERY_SLICE {
        int new_y = ((3 * y) + (2 * x)) % STATE_Y_LEN;
        size_t s_new_index = get_3d_index(slice_num, new_y, y);
        size_t s_index = get_3d_index(slice_num, y, x);
        s_new.array[s_new_index] = s->array[s_index];
      }
    }
  }
  return s_new;
}

void chi(state *s) {
  for (int i = 0; i < STATE_SIZE_BYTES; i++) {
    size_t i_below = (i + STATE_X_LEN) % STATE_SIZE_BYTES;
    size_t i_below_below = (i + (2 * STATE_X_LEN)) % STATE_SIZE_BYTES;
    s->array[i] ^= (~s->array[i_below] & s->array[i_below_below]);
  }
}

void iota(state *state, int round) {
  for (int m = 0; m < L; m++) {
    size_t state_bit_index = ((u_int64_t)powl(2, m) - 1) % WORD;
    size_t lsft_bit_index = (m + (7 * round)) % WORD;
    u_int64_t lsfr = xorshift(round * (u_int64_t)m);

    u_char lsfr_bit = (lsfr >> (64 - lsft_bit_index)) & 1;

    size_t s_byte_index = state_bit_index / 8;
    size_t s_bit_index = state_bit_index % 8;

    u_char *byte = &state->array[get_3d_index(s_byte_index, 0, 0)];
    lsfr_bit <<= (8 - state_bit_index);
    *byte ^= lsfr_bit;
  }
}

u_int64_t xorshift(u_int64_t xs) {
  xs ^= xs << 7;
  xs ^= xs >> 9;
  xs ^= xs << 8;
  return xs;
}

void padd_input(u_char *buffer, size_t buff_len) {
  int str_len = strnlen((char *)buffer, buff_len);
  int padding_needed = buff_len - str_len;
  if (padding_needed < 0) {
    printf("String too long!\n");
    exit(EXIT_FAILURE);
  }

  if (padding_needed == 1) {
    buffer[buff_len - 1] = 0x81; // 1000 0001
    return;
  }

  memset(buffer + str_len, 0, padding_needed);
  buffer[str_len] = 0x80;
  buffer[buff_len - 1] = 0x01;
}

/// XORs bit from b2[bit\_b2] to b1[bit\_b1]
void set_bit(u_char *b1, u_char *b2, int bit_b1, int bit_b2) {
  u_char bit = *b2 << bit_b2;
  bit &= 0x80; // leaving only one bit
  bit >>= bit_b1;
  *b1 ^= bit;
}

u_char *unswapp_bits(u_char *state_part, size_t len) {
  u_char *result = malloc(len);
  memset(result, 0, len);
  for (int i = 0; i < len * 8; i++) {
    int byte_index = i / 8;
    int bit_index = i % 8;
    int state_slice = byte_index / STATE_SLICE_LEN;
    int state_byte = i % STATE_SLICE_LEN;
    int state_bit = (i / STATE_SLICE_LEN) % 8;
    set_bit(&result[byte_index],
            &state_part[state_slice * STATE_SLICE_LEN + state_byte], bit_index,
            state_bit);
  }
  return result;
}

void swapp_bits(state *s, u_char *unswapped, size_t len) {
  for (int i = 0; i < len * 8; i++) {
    int byte_index = i / 8;
    int bit_index = i % 8;
    int state_slice = byte_index / STATE_SLICE_LEN;
    int state_byte = i % STATE_SLICE_LEN;
    int state_bit = (i / STATE_SLICE_LEN) % 8;
    set_bit(&(s->array[state_slice * STATE_SLICE_LEN + state_byte]),
            &unswapped[byte_index], state_bit, bit_index);
  }
}
