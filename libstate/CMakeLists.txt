cmake_minimum_required(VERSION 3.22)

add_library(libstate "libstate.h" "libstate.c")

add_executable(tests tests.c)
target_link_libraries(tests m ${CRITERION_LIBRARIES})
add_test(libstate tests)

target_link_libraries(libstate m)
