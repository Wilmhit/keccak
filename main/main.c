#include "../libstate/libstate.h"
#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

u_char *run_keccak_on(u_char *line, size_t line_size);
state absorb(u_char *line, size_t line_size);
u_char *squeeze(state *s);
void save_hash(u_char *hash);

int main(int argc, char **argv) {
  // Open file
  FILE *file = fopen("input.bin", "r");
  if (file == NULL) {
    printf("Could not load file\n");
    return EXIT_FAILURE;
  }

  // Read first line of a file
  ssize_t read_bytes;
  char *buffer = NULL;
  size_t buff_size = 0;
  if ((read_bytes = getline(&buffer, &buff_size, file))) {
    fclose(file);
    u_char *hash = run_keccak_on((u_char *)buffer, buff_size);
    save_hash(hash);
    return EXIT_SUCCESS;
  } else {
    fclose(file);
    printf("Could not read contents of a file\n");
    return EXIT_FAILURE;
  }
}

void save_hash(u_char *hash) {
  FILE *file = fopen("output.bin", "w");
  if (file == NULL) {
    printf("Could not write file\n");
    exit(EXIT_FAILURE);
  }
  fwrite(hash, OUTPUT_SIZE_BITS / 8, 1, file);
  fclose(file);
}

u_char *run_keccak_on(u_char *line, size_t line_size) {
  line = realloc(line, line_size + 1);
  line[line_size] = 0;
  line_size++;
  padd_input(line, line_size);

  state s = absorb(line, line_size);
  return squeeze(&s);
}

u_char *squeeze(state *s) {
  size_t result_len = OUTPUT_SIZE_BITS / 8;
  return unswapp_bits(s->array, result_len);
}

state absorb(u_char *line, size_t line_size) {
  int iterations_needed = line_size / R_BYTES;
  if (line_size % R_BYTES != 0) {
    iterations_needed++;
  }

  state s = get_state(R_BITS);
  for (int i = 0; i < iterations_needed; i++) {
    absorbing_iteration(&s, line + (i * R_BYTES));
  }
  return s;
}
