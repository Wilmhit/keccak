cmake_minimum_required(VERSION 3.22)

set(CMAKE_EXPORT_COMPILE_COMMANDS on)
set(CMAKE_BUILD_TYPE Debug)

project(
  Keccak
  VERSION 1.0
  LANGUAGES C
)

# Finding criterion
find_package(PkgConfig)
find_path(
  CRITERION_INCLUDE_DIR criterion/criterion.h
  PATH_SUFFIXES criterion
)
find_library(CRITERION_LIBRARY NAMES criterion libcriterion)
set(CRITERION_LIBRARIES ${CRITERION_LIBRARY})
set(CRITERION_INCLUDE_DIRS ${CRITERION_INCLUDE_DIR})
include(FindPackageHandleStandardArgs)
# message(${CRITERION_LIBRARY})
# message(${CRITERION_INCLUDE_DIRS})
find_package_handle_standard_args(
  Criterion DEFAULT_MSG
  CRITERION_LIBRARY CRITERION_INCLUDE_DIR
)
mark_as_advanced(CRITERION_INCLUDE_DIR CRITERION_LIBRARY)

if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
  enable_testing()
endif()

add_subdirectory(libstate)
add_subdirectory(main)
file(COPY_FILE build/compile_commands.json compile_commands.json)
